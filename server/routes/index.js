var express = require('express');
var router = express.Router();
var render_multi_url = require('../scripts/render_multi_url');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });

});

module.exports = router;
